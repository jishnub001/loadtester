
"""Script file which perform load testing"""

'''
    File name: loadtester.py
    Author: Jishnu B
    Date created: 28/07/2017
    Python Version: 3.5
     '''

#Import neccessary modules
import threading
from modules.loadtester import LoadTester
from modules.config import *
import time

#calculating the script start time
start_time = time.time()

#Function to delete the content from result file
print("[+]Staring AHIS Load tester")
def deleteContent(fName):
	try:
		with open(fName, "w"):
			pass
	except Exception as e:
		print(e)

#Worker function which will run under diff threads
def worker(threadid,test):
    """thread worker function"""
    #Calling the test method form Load Tester class
    time=test.test(SERVICE_CENTER,SERVICE_POINT,NURSING_STATION,INSTITUTION,IP_REG,REGISTRATION_COUNTER,BILLING_COUNTER,CASH_COUNTER,FREQ)
    
    #Saving each thread result to a file
    if not time:

        try:
            with open("failed_threads.txt","a") as f:
                f.write("Thread %s :%s \n" %(threadid,"Location selection failed"))
        except Exception as e:
            print(e)
            #Exit app in case of exception
            exit()
    elif type(time) is not dict:
        try:
            with open("failed_threads.txt","a") as f:
                f.write("Thread %s :%s \n" %(threadid,time))
        except Exception as e:
            print(e)
            #Exit app in case of exception
            exit()

    else:
        try:
        	with open("result.txt","a") as f:
        		f.write("Thread %s Total Time:%s \n" %(threadid,time['total_time']))
        except Exception as e:
        	print(e)
            #Exit app in case of exception
        	exit()

        try:
            with open("request_failed.txt","a") as f:
                f.write("Thread %s Failed :%s \n" %(threadid,time['failed']))
        except Exception as e:
            print(e)
            #Exit app in case of exception
            exit()



#Delete content of file
print("[+]Creating result and failed Report")
deleteContent("result.txt")
deleteContent("request_failed.txt")
deleteContent("failed_threads.txt")


#Object intialisation
test=LoadTester(USERNAME,PASSWORD,URL)

#Creaing multiple threads as configured in the Config
print("[+]Staring %d threads" %(WORKER))

threads = []
 #Creating thread for worker function
for i in range(int(WORKER)):
    t = threading.Thread(target=worker,args=(i,test,))
    threads.append(t)
    t.start()


#Variable for total time calculation
totaltime=0
failedreq=0
#Loop to wait until all workers are finished

print("[+]Worker Execution in progress")
for x in threads:
     x.join()

print("[+]Job finished Calculating the summary")
end_time=time.time() - start_time

#Opening the result.txt and calculating total time from all threads
try:
    with open("result.txt") as f:
        #Readline by line and split after : till white space
        for line in f:
            time=line[line.find(":")+1:].split()[0]
            #Calculating the total time
            totaltime=totaltime+float(time)
except Exception as e:
    print("[+]Couldn't calaculate the Total response")

#Opening the result.txt and calculating total time from all threads
try:
    with open("request_failed.txt") as f:
        #Readline by line and split after : till white space
        for line in f:
            req=line[line.find(":")+1:].split()[0]
            #Calculating the total time
            failedreq=failedreq+int(req)
except Exception as e:
    print("[+]Couldn't calaculate the Total Failed Request")



#Calculating total requests sent prt thread ie Loop Frequencty * total urls
total_req=FREQ * 10
#Total requests from all threads ie workers * total req per thread
overal_requests=WORKER * total_req

print("[+]Writing the summary")
#Writing the summary to text file
try:
    with open("result.txt" ,"a") as f:
        f.write("\n")
        f.write("###Summary#####\n")
        f.write("Total Request sent per thread : %s \n" %(total_req))
        f.write("Total Request sent over using %s threads : %s \n" %(WORKER,overal_requests))
        f.write("Total response Time taken from %s threads : %s secs \n" %(WORKER,totaltime))
except Exception as e:
    print(e)
    print("Couldn't add Summary")

print("[+]Result Saved in result.txt")

try:
    with open("request_failed.txt" ,"a") as f:
        f.write("\n")
        f.write("###Summary#####\n")
        f.write("Total failed requests : %s \n" %(failedreq))
except Exception as e:
    print(e)
    print("Couldn't add Summary")

print("[+]Result Saved in result_failed.txt")


