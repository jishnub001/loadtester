
"""MAin configuration file for the load tester script"""

'''
    File name: config.py
    Author: Jishnu B
    Date created: 28/07/2017
    Python Version: 3.5
     '''

#AHIS user name and password
'''
PLEASE NOTE : The user should have adt test role and mrdv test role mapped
The follwing should be assigned to the user
service center 
service point 
Nursing Station 
REGISTRATION_COUNTER 
BILLING_COUNTER
CASH_COUNTER
INSTITUTION
'''
#Username@ sitecode
USERNAME="adt@ems"
#password
PASSWORD="aa"
#Web Server IP.If the port is othjer than 80 then add like 192.168.181.144:8000
URL="192.168.181.144:8080"

#Table ID of any of the Service center mapped to the user
SERVICE_CENTER=48

#Table ID of any of the Service point mapped to the user
SERVICE_POINT=1

#Table ID of any of the NS mapped to the user
NURSING_STATION=1

#Table ID of any of the REgistration Counter mapped to the user
REGISTRATION_COUNTER=2

#Table ID of Billing COunter mapped to the user
BILLING_COUNTER=13

#Table ID of Cash counter mapped to the user
CASH_COUNTER=14

#Institution ID
INSTITUTION=1

#Dont change
IP_REG=25


#Freq decides the number of times the url request should be send.20 means each url will be accessed 20 times
FREQ=50

#Worker decides the number of threads needed to create for the tool.Each thread will act as an individual program
WORKER=450