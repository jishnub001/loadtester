
"""JLoad Tester MAin class"""

'''
    File name: loadtester.py
    Author: Jishnu B
    Date created: 28/07/2017
    Python Version: 3.5
     '''
#importing request module
import requests

class LoadTester:
	"""Main class which will do load testing"""

	def __init__(self,user,passwd,url):
		self.user=user
		self.passwd=passwd
		self.url="http://"+url

	def test(self,sc,sp,ns,ins,ip,rc,bc,cc,freq):
		'''Method which will do the requests'''
		#for calculating the total response time
		total_time=0
		failed=0
		total_reqs=0

		#Session object of requests
		session = requests.session()
		#payload creation for authentication
		payload = {'j_username': self.user, 'j_password': self.passwd}

		#Headers for request for impersonate browsers
		headers = {
		'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36'
		}

		#Starting the exception catcher

		try:
			#Loading the login page using session and calculating response time
			login1=session.get("%s/his/Jsp/Common/index.jsp" %(self.url))
			total_time=total_time+ login1 .elapsed.total_seconds()
			#Performing login and calucating time
			login2 =session.post(url='%s/his/j_security_check' %(self.url), data=payload,headers=headers)
			total_time=total_time+ int(login2.elapsed.total_seconds())

			#If response is of location selector . Then perform post ,with selected data and calclate repsonse  
			if "Location Selector" in login2.text:
				
				payload2={
				"htmlPageTopContainer_container_list_1":sc,
				"htmlPageTopContainer_container_list_2":sp,
				"htmlPageTopContainer_container_list_3":ns,
				"htmlPageTopContainer_container_list_4":ins,
				"htmlPageTopContainer_container_list_5":ip,
				"htmlPageTopContainer_container_list_6":rc,
				"htmlPageTopContainer_container_list_7":bc,
				"htmlPageTopContainer_container_list_8":cc,
				"Page_refindex_hidden":1,
				"htmlPageTopContainer_action":"MULTILOCATION-SET_IN_SESSION"}

				loc= session.post(url='%s/his/Jsp/Core_Common/MultiLocation.jsp?action=MULTILOCATION-SHOW_ALL_LOCATIONS&index=true' %(self.url),data=payload2,headers=headers)

				#If response after post is of home page then proceed with acessing other pages and calculate the time
				if "Home" in loc.text:
					total_time=total_time+ int(loc.elapsed.total_seconds())
				else:
					return False

		except Exception as e:
			#Return error incase of eception
			print(e)
			return e

		else:
			for x in range(int(freq)):
				reg=session.get('%s/his/Jsp/adt/PatientRegistration.jsp?action=ADT-NEW_REGISTRATION&isMenu=true' %(self.url),headers=headers)
				if "Patient Registration" in reg.text:
					total_time=total_time+ reg.elapsed.total_seconds()
				else:
					print("Get request on Patient registration failed .response code : %s " %(reg.status_code))
					failed=failed+1

				#Acess Appointment page
				app=session.get('%s/his/Jsp/adt/AppointmentBrowser.jsp?action=ADT-APPOINTMENT_BROWSER_SHOW_APPOINTMENTS&isMenu=true' %(self.url),headers=headers)
				if "Appointment" in app.text:
					total_time=total_time+ int(app.elapsed.total_seconds())
				else:
					print("Get request on Appointment Browser failed response code : %s " %(app.status_code))
					failed=failed+1

				#Acess Encounter Page

				enc=session.get('%s/his/Jsp/adt/Encounter.jsp?action=ADT-SHOW_ENCOUNTER&isMenu=true' %(self.url),headers=headers)
				if "Encounter" in enc.text:
					total_time=total_time + int(enc.elapsed.total_seconds())
				else:
					print("Get request on Encounter failed response code : %s " %(enc.status_code))
					failed=failed+1


				#Acess Mrd Viewer Page

				mrdv=session.get('%s/his/Jsp/mrdviewer/mrdsearchNew.jsp?action=MRDVIEWER-SEARCH_MRD_NEW&isMenu=true' %(self.url),headers=headers)
				if "New Mrd Viewer" in mrdv.text:
					total_time=total_time + int(mrdv.elapsed.total_seconds())
				else:
					print("Get request on mrdv  failed response code : %s " %(mrdv.status_code))
					failed=failed+1
					

				#Acess OpBill Page

				opbill=session.get('%s/his/Jsp/billing/FinalBilling.jsp?action=BILL-VIEW_OP_BILLING&isMenu=true' %(self.url),headers=headers)
				if "OP Bill" in opbill.text:
					total_time=total_time + int(opbill.elapsed.total_seconds())
				else:
					print("Get request on OPBill failed response code : %s " %(opbill.status_code))
					failed=failed+1

				#Acess Admission request Page
				adm=session.get('%s/his/Jsp/adt/AdmissionPlan.jsp?action=ADT-SHOW_ADMISSION_PLAN&isMenu=true' %(self.url),headers=headers)
				if "Admission Request" in adm.text:
					total_time=total_time + int(adm.elapsed.total_seconds())
				else:
					print("Get request on AdmissionPlan failed response code : %s " %(adm.status_code))
					failed=failed+1

				#Acess IP Patient Browser Page
				ipb=session.get('%s/his/Jsp/nursing/MyPatient.jsp?action=NS-IP_MYPATIENTS_INITIALIZE&isMenu=true' %(self.url),headers=headers)
				if "IP Patients" in ipb.text:
					total_time=total_time + int(ipb.elapsed.total_seconds())
				else:
					print("Get request on Patient Browser failed response code : %s " %(ipb.status_code))
					failed=failed+1


				#Access Package Form MAp

				pkm=session.get('%s/his/Jsp/emr/PackageFormMap.jsp?action=EMR-PACKAGE_FORM_MAP_INIT_PAGE&isMenu=true' %(self.url),headers=headers)
				if "Package Form Map" in pkm.text:
					total_time=total_time + int(pkm.elapsed.total_seconds())
				else:
					print("Get request on Package form map failed response code : %s " %(pkm.status_code))
					failed=failed+1

				#Acess LIS result processing 

				lab=session.get('%s/his/Jsp/lis/ResultProcess.jsp?action=LAB-INITIALIZE_RESULT&isMenu=true' %(self.url),headers=headers)
				if "LIS-Result Processor" in lab.text:
					total_time=total_time + int(lab.elapsed.total_seconds())
				else:
					print("Get request on LIS Result processing failed response code : %s " %(lab.status_code))
					failed=failed+1

				#Acess Advanced PAtient Search Page

				aps=session.get('%s/his/Jsp/adt/PatientCompleteSearch.jsp?action=ADT-INIT_COMPLETE_PATIENT_SEARCH&isMenu=true' %(self.url),headers=headers)
				if "Advanced Patient" in aps.text:
					total_time=total_time + int(aps.elapsed.total_seconds())
				else:
					print("Get request on ADV Patient Search failed response code : %s " %(aps.status_code))
					failed=failed+1

			#Returning the Total response time 
			return {"total_time":total_time,"failed":failed}











